"use client";
import Link from "next/link";
import React from "react";
import Image from "next/image"
import Box from '@mui/material/Box';
import { DataGridPro } from '@mui/x-data-grid-pro';
import {createTheme, ThemeProvider} from "@mui/material";

const theme = createTheme({
    palette: {
        text: {
            primary: "#FFFFFF",
        }
    },
    typography: {
        fontFamily: "var(--font-inter)",
        // In Chinese and Japanese the characters are usually larger,
        // so a smaller fontsize may be appropriate.
        fontSize: 45,
    },
});

export default function Page(){
    return (
            <div className={" p-3 fixed z-50 text-8xl inset-y-0 my-auto h-96 inset-x-0 mx-auto w-120"}>
                <div className={"h-24"}></div>


                <div className={" bg-slate-400 opacity-50 fixed rounded-xl h-135 text-center inset-x-0 mx-auto w-120"}>
                    <div className={"opacity-100"}>
                        <div className={"h-8 opacity-100"}></div>
                    </div>
                </div>

                <div className={" font-sans text-white fixed rounded-xl h-135 text-center inset-x-0 mx-auto w-120 leading-tight"}>
                    <div className={"opacity-100"}>
                        <div className={"h-8 opacity-100"}></div>
                        <Link href = {"/"} className={"opacity-100"}>{"<--"}</Link> <br/>
                        <ThemeProvider theme={theme}>
                        <div className={""}>
                        <Box sx ={{height: 750, width: "100%"}}>
                        <DataGridPro rows = {[{id: "oct22", amount: "$2,000", status: "complete"}]} columns={[{field: "id", width: 300}, {field: "status", width:300},{field: "amount", width: 300}]}/>
                         </Box>
                        </div>
                        </ThemeProvider>


                    </div>
                </div>



            </div>
            )
}