"use client";
import Link from "next/link";
import React from "react";
import Image from "next/image"

import { styled } from '@mui/material/styles';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';

import PropTypes from 'prop-types';
import Slider, { SliderThumb } from '@mui/material/Slider';
import Tooltip from '@mui/material/Tooltip';
import Box from '@mui/material/Box';

export default function Page(){
    return (
            <div className={" p-3 fixed z-50 text-8xl inset-y-0 my-auto h-96 inset-x-0 mx-auto w-120"}>
                <div className={"h-24"}></div>


                <div className={" bg-slate-400 opacity-50 fixed rounded-xl h-135 text-center inset-x-0 mx-auto w-120"}>
                    <div className={"opacity-100"}>
                        <div className={"h-8 opacity-100"}></div>
                    </div>
                </div>

                <div className={" font-sans text-white fixed rounded-xl h-135 text-center inset-x-0 mx-auto w-120 leading-tight"}>
                    <div className={"opacity-100"}>
                        <div className={"h-8 opacity-100"}></div>
                        <Link href = {"/"} className={"opacity-100"}>{"<--"}</Link> <br/>
                        <Link href = {"/"} className={"opacity-100"}>{"autopay: active"}</Link> <br/>

                        <FormControlLabel
                            control={<IOSSwitch sx={{ m: 1 }} defaultChecked />}
                            label="autopay setting" size={"large"}
                        />
                        

                        </div>
                    </div>



            </div>
    )
}

const IOSSwitch = styled((props) => (
        <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
        ))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    '& .MuiSwitch-switchBase': {
        padding: 0,
        margin: 2,
        transitionDuration: '300ms',
        '&.Mui-checked': {
            transform: 'translateX(16px)',
            color: '#fff',
            '& + .MuiSwitch-track': {
                backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
                opacity: 1,
                border: 0,
            },
            '&.Mui-disabled + .MuiSwitch-track': {
                opacity: 0.5,
            },
        },
        '&.Mui-focusVisible .MuiSwitch-thumb': {
            color: '#33cf4d',
            border: '6px solid #fff',
        },
        '&.Mui-disabled .MuiSwitch-thumb': {
            color:
            theme.palette.mode === 'light'
            ? theme.palette.grey[100]
            : theme.palette.grey[600],
        },
        '&.Mui-disabled + .MuiSwitch-track': {
            opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
        },
    },
    '& .MuiSwitch-thumb': {
        boxSizing: 'border-box',
        width: 22,
        height: 22,
    },
    '& .MuiSwitch-track': {
        borderRadius: 26 / 2,
        backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
        opacity: 1,
        transition: theme.transitions.create(['background-color'], {
            duration: 500,
        }),
    },
}));