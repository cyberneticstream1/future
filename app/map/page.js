"use client";
import Script from "next/script";
import * as React from 'react';

export default function Map(props){

    const main = async() => {
        await setupMapKitJs();

        const bayArea = new mapkit.CoordinateRegion(
                new mapkit.Coordinate(37.628724, -122.195537),
                new mapkit.CoordinateSpan(0.6, 0.6)
                );

        const map = new mapkit.Map("map-container");
        map.mapType = mapkit.Map.MapTypes.Satellite
        map.region = bayArea;

        const property = new mapkit.Coordinate(37.628724, -122.195537);
        const propertyAnnotation = new mapkit.MarkerAnnotation(property);
        propertyAnnotation.color = "#969696";
        propertyAnnotation.selected = "true";
        propertyAnnotation.glyphText = "🛩️";

        map.addItems([ propertyAnnotation]);
    };

    const setupMapKitJs = async() => {
        if (!window.mapkit || window.mapkit.loadedLibraries.length === 0) {
            await new Promise(resolve => { window.initMapKit = resolve });
            delete window.initMapKit;
        }
    const jwt = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IllIWlgzNjlHN0gifQ.eyJpc3MiOiJRUzhTM01LVTZMIiwiaWF0IjoxNjY3OTcwNTU2LCJleHAiOjE2NzA1NjI1MDR9.86HtzzR6G-Cb4mluBQ9YkBrIBlOMCpZA_zNWGR_en_shRinfy8DDyCgGOwHmpXQU_qr1wTDIgwFRqA5NpSub3Q";
        mapkit.init({
            authorizationCallback: done => { done(jwt); }
        });
    };
    return(
             <div  >
            <Script src="https://cdn.apple-mapkit.com/mk/5.x.x/mapkit.core.js" crossorigin async data-callback="initMapKit" data-libraries="map,annotations,services" data-initial-token="" onReady={()=> {main();}}></Script>
                 <div id="map-container" className={"map w-fill h-screen" }></div>
            </div>
    )
}



